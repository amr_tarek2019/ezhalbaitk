<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



// user routes app

Route::group(['prefix'=>'user','namespace'=>'Api\user'], function (){
    Route::group(['prefix'=>'About'],function () {
        Route::get('', 'AboutController@AboutForm');
    });

    Route::group(['prefix'=>'Categories'],function () {
        Route::get('', 'CategoriesController@CategoryForm');
    });

    Route::group(['prefix'=>'Cities'],function () {
        Route::get('', 'CitiesController@CityForm');
    });

    Route::group(['prefix'=>'Subcategories'],function () {
        Route::post('', 'SubcategoriesController@SubcategoriesForm');
    });

    Route::group(['prefix'=>'Contacts'],function () {
        Route::post('', 'ContactsController@createContacts');
    });

    Route::group(['prefix'=>'Packages'],function () {
        Route::get('', 'PackagesController@PackagesForm');
    });

    Route::group(['prefix'=>'Profile'],function () {
        Route::get('GetData', 'ProfileController@GetProfileDataForm');
        Route::post('Update', 'ProfileController@updateProfile');
        Route::post('GetPassword', 'ProfileController@getPasswordProfile');
        Route::post('UpdatePassword', 'ProfileController@updatePasswordProfile');
    });

    Route::group(['prefix'=>'Authentication'],function () {
        Route::post('Registration', 'AuthenticationController@registration');
        Route::post('Login','AuthenticationController@login');
        Route::post('Location','AuthenticationController@location');
        Route::post('ForgetPassword', 'AuthenticationController@forgetPassword');
        Route::post('VerificationCode', 'AuthenticationController@verification');
        Route::post('ChangePassword', 'AuthenticationController@changePassword');
        Route::post('SocialLogin', 'AuthenticationController@socialLogin');
    });

    Route::group(['prefix'=>'Subscriptions'],function () {
        Route::get('', 'SubscriptionsController@subscriptionsForm');
        Route::post('Reserve', 'SubscriptionsController@createSubscription');
        Route::post('UserShow', 'SubscriptionsController@showUserSubscriptions');
        Route::post('Update', 'SubscriptionsController@updateSubscription');
        Route::post('NextVisit', 'SubscriptionsController@subscriptionNextVisitStatus');
        Route::post('InProgress', 'SubscriptionsController@subscriptionInProgressStatus');
        Route::post('Completed', 'SubscriptionsController@subscriptionCompletedStatus');
        Route::post('RateTechnician', 'SubscriptionsController@rateTechnician');
    });

    Route::group(['prefix'=>'Order'],function () {
        Route::post('','OrderController@createOrder');
        Route::get('Status','OrderController@orderStatus');
        Route::get('JobDetails','OrderController@JobDetails');
    });

    Route::group(['prefix'=>'Rate'],function () {
        Route::post('Technician','RateTechnicianController@createRate');
    });

    Route::group(['prefix'=>'Notifications'],function () {
        Route::get('','NotificationsController@notificationsForm');
    });

    Route::group(['prefix'=>'History'],function () {
        Route::post('', 'HistoryController@index');
    });

    Route::group(['prefix'=>'Units'],function () {
        Route::get('', 'UnitsController@unitsForm');
    });

    Route::group(['prefix'=>'Filter'],function () {
        Route::get('', 'FilterationController@FilterForm');
    });
});
