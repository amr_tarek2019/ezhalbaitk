<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSubcategory extends Model
{
    protected $table='order_subcategory';
    protected $fillable=['order_id', 'subcategory_id','quantity','total'];
    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
