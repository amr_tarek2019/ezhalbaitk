<?php

namespace App\Http\Controllers\Api\User;

use App\Category;
use App\CategoryPackage;
use App\Contact;
use App\RateTechnicianSubscription;
use App\Subscription;
use App\User;
use App\UserSubscription;
use App\UserTechnicianSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscriptionsForm(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $subscriptions= Subscription::where('category_id', $request->category_id)->where('status', 1)
            ->get();
        $res_item = [];
        $res_list = [];
        foreach ($subscriptions as $res) {
            $res_item['id'] = $res->id;
            $res_item['months'] = $res->months;
            $res_item['price'] = $res->price;
            $res_item['currency'] = $res->currency;
            $category=Category::where('id', $res->id)->select('name_'.$lang.' as name')->first();
            $res_item['category']=$category;
            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of subscriptions successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSubscription(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $subscription = Subscription::where('id',$request->subscription_id)->select('id')->first();
        $price = Subscription::where('id',$request->subscription_id)->select('price')->first();
        $months=Subscription::where('id',$request->subscription_id)->select('months')->first();
        $totalPrice=$months['months']*$price['price'];
        $order_number = rand(111111111, 999999999);
        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'time'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $reserveSubscription=new UserSubscription();
        $reserveSubscription->user_id=$user->id;
        $reserveSubscription->subscription_id=$subscription->id;
        $reserveSubscription->subscription_number=$order_number;
        $reserveSubscription->date=$request->date;
        $reserveSubscription->time=$request->time;
        $reserveSubscription->total_price=$totalPrice;
        $reserveSubscription->save();
        $response=[
            'message'=>'package submitted successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showUserSubscriptions(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $subscriptions = UserSubscription::where('user_id', $user->id)->get();


        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            $res_item['id'] = $res->id;

           $cat=Subscription::where('id',$res->id)->select('category_id')->first();
           $category=Category::select('name_'.$lang.' as name')->where('id',$res->id)->first();
            $res_item['category'] = $category;
            $userData=User::where('jwt_token', $jwt)->select('name','address','phone','email')->first();
            $res_item['user']=$userData;
            $res_item['date'] = $res->date;
            $res_item['time'] = $res->time;
            $res_item['subscripton_price'] = $res->total_price;
            $res_item['currency'] = $res->currency;
            $res_list[] = $res_item;
        }
        $response = [
            'message' => 'get data of User subscriptions successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSubscription(Request $request)
    {
        $userSubscription=UserSubscription::where('id',$request->id)->first();
        $subscription = Subscription::where('id',$request->subscription_id)->select('id')->first();
        $price = Subscription::where('id',$request->subscription_id)->select('price')->first();
        $months=Subscription::where('id',$request->subscription_id)->select('months')->first();
        $totalPrice=$months['months']*$price['price'];
        $order_number = rand(111111111, 999999999);
        $validator = Validator::make($request->all(), [
            'date'=>'required',
            'time'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        $userSubscription->subscription_id=$subscription->id;
        $userSubscription->subscription_number=$order_number;
        $userSubscription->date=$request->date;
        $userSubscription->time=$request->time;
        $userSubscription->total_price=$totalPrice;
        $userSubscription->save();
        $response=[
            'message'=>'package updated successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function subscriptionNextVisitStatus(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $subscriptions = UserSubscription::where('user_id', $user->id)
            ->where('status','0')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            $res_item['id'] = $res->id;
            $subscriptionMonth = Subscription::where('id',$res->id)->select('months')->first();
            $res_item['months']=$subscriptionMonth;
            $res_item['date'] = $res->date;
            $res_item['time'] = $res->time;
            $res_item['subscripton_number'] = $res->subscription_number;
            $res_list[] = $res_item;
        }
        $response = [
            'message' => 'get data of User next visits subscriptions successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function subscriptionInProgressStatus(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $subscriptions = UserSubscription::where('user_id', $user->id)
            ->where('status','1')->where('accepted','1')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            $res_item['id'] = $res->id;
            $subscriptionMonth = Subscription::where('id',$res->id)->select('months')->first();
            $res_item['months']=$subscriptionMonth;
            $res_item['date'] = $res->date;
            $res_item['time'] = $res->time;
            $res_item['subscripton_number'] = $res->subscription_number;
            $res_list[] = $res_item;
        }
        $response = [
            'message' => 'get data of User next visits subscriptions successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function subscriptionCompletedStatus(Request $request)

    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $subscriptions = UserSubscription::where('user_id', $user->id)
            ->where('status','2')->where('accepted','1')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($subscriptions as $res) {
            $res_item['id'] = $res->id;
            $subscriptionMonth = Subscription::where('id',$res->id)->select('months')->first();
            $res_item['months']=$subscriptionMonth;
            $res_item['date'] = $res->date;
            $res_item['time'] = $res->time;
            $res_item['subscripton_number'] = $res->subscription_number;
            $res_list[] = $res_item;
        }
        $response = [
            'message' => 'get data of User next visits subscriptions successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }


    public function rateTechnician(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $technician=UserTechnicianSubscription::where('user_subscription_id',$request->technician_id)->pluck('technician_id')->first();
        $order=UserTechnicianSubscription::where('id',$request->subscription_id)->first();
        $validator = Validator::make($request->all(), [
            'rate'=>'required',
            'review' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $technicanRate=new RateTechnicianSubscription();
        $technicanRate->user_id=$user->id;
        $technicanRate->technician_id=$technician;
        $technicanRate->user_subscription_id=$order->user_subscription_id;
        $technicanRate->rate = $request->rate;
        $technicanRate->review = $request->review;
       // dd($request->all());
        $technicanRate->save();
        $response=[
            'message'=>'technical rate created successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }
}
