<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\BaseController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetProfileDataForm(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['address'] = $user['address'];
        if ($user)
        {
            $response=[
                'message'=>'get data of user profile successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'api.something went wrong',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'address'=>'required',
            'phone'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->name = $request->name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->image=$request->image;
        $user->address=$request->address;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['address'] = $user['address'];
            $response=[
                'message'=>'profile changed successfully',
                'status'=>200,
                'data'=> $data
            ];

        }
        return \Response::json($response,200);
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>'submitted before',
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPasswordProfile(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $data['id'] = $user['id'];
        $data['password'] = $user['password'];
        if ($user)
        {
            $response=[
                'message'=>'get data of user profile successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'somethingwentwrong',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function updatePasswordProfile(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'password'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->password = $request->password;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['password'] = $user['password'];
            $response=[
                'message'=>'profile changed successfully',
                'status'=>200,
                'data'=> $data
            ];

        }
        return \Response::json($response,200);
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>'submitted before',
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
