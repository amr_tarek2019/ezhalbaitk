<?php

namespace App\Http\Controllers\Api\User;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\RateTechnician;
use App\Technician;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FilterationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     */
    public function FilterForm(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $categories=Category::select('id','name_'.$lang. ' as name')
            ->where('status',1)->get();





        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'lng' => 'required',
            'lat' => 'required',
            'rate'=> 'required',
            'cat_id'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $catTechnicians = Technician::where('id',$request->cat_id)->get();
        $res_item = [];
        $res_cat_list  = [];
        foreach ($catTechnicians as $res) {
            $res_item['id'] = $res->id;
            $res_item['technician']=Technician::where('id',$res->id)->select('id','subcategory_id')->first();
            $technician = \App\Technician::where('id',$res->id)->select('id','user_id')->first();
            $res_item['name'] = User::where('id',$technician->user_id)->where('user_type','technician')->select('name','image')->first();

            $res_cat_list[] = $res_item;
        }





        $lng = $request->lng;
        $lat = $request->lat;

//        $rateings = User::whereBetween('rate',[$request->rate_from,$request->rate_to])->select('id','name'
//            ,'lat','lng','image')->get();


        $technician_list=User::leftJoin('technicians', 'users.id', '=', 'technicians.user_id')
            ->selectRaw("( FLOOR(3959 * ACOS( COS( RADIANS( $lat) ) * COS( RADIANS( lat ) ) * COS( RADIANS( lng ) - RADIANS($lng) )
             + SIN( RADIANS($lat) ) * SIN( RADIANS( lat ) ) )) )
             distance,users.id,users.image,users.lat,users.lng,technicians.subcategory_id")->havingRaw("distance <= 50")
            ->where("status", 1)
            ->where('user_type','technician')
            ->orderBy('id', 'desc')
            ->get();

        $rateings = RateTechnician::where('rate',$request->rate)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($rateings as $res) {
            $res_item['id'] = $res->id;
            $res_item['rate']=$res->rate;
            $res_item['technician']=Technician::where('id',$res->technician_id)->select('id')->first();
            $technician = \App\Technician::where('id',$res->technician_id)->select('id','user_id')->first();
            $res_item['technician_name'] = \App\User::where('id',$technician->user_id)->where('user_type','technician')->select('name','image')->first();

            $res_list[] = $res_item;
        }

        $data['categories']=$res_cat_list;
        $data['search_rates'] = $res_list;
        $data['technician']=$technician_list;

        $response=[
            'message'=>'search results',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);






    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
