<?php

namespace App\Http\Controllers\Api\User;

use App\Cart;
use App\Http\Controllers\Api\BaseController;
use App\JobDetails;
use App\Order;
use App\OrderImage;
use App\OrderRequest;
use App\OrderSubcategory;
use App\PushNotification;
use App\Subcategory;
use App\Technician;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createOrder(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'payment_type' => 'required',
            'city_id'=>'required',
            'date' => 'required',
            'time' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'area' => 'required',
            'block' => 'required',
            'street' => 'required',
            'house' => 'required',
            'floor' => 'required',
            'appartment' => 'required',
            'directions' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }



        $order= Order::create(array_merge($request->all(),[
            'user_id'=>$user->id,
        ]));
        $order->save();

        $validator = Validator::make($request->all(), [
            'subcategory_id' => 'required',
            'quantity'=>'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $price = Subcategory::where('id',$request->subcategory_id)->select('price')->first();
        for($i=0;$i<count($request->subcategory_id);$i++)
        {
            $quantity=$request->quantity[$i];
            $total=$quantity*$price['price'];
            $orders=new OrderSubcategory();
            $orders->order_id=$order->id;
            $orders->subcategory_id=$request->subcategory_id[$i];
            $orders->total=$total;
            $orders->quantity=$quantity;
            $orders->save();
        }
        $response=[
            'status'=>200,
            'message'=>'job successfully posted',
        ];
        return \Response::json($response,200);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderStatus(Request $request)
    {
        $order=Order::where('id',$request->order_id)->select('status')->first();
        $data['job_status'] = $order['status'];
        if ($order)
        {
            $response=[
                'message'=>'get status of job successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function JobDetails(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jobDetails=JobDetails::where('id',$request->order_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($jobDetails as $res) {
            $res_item['id'] = $res->id;

            $technicianId=JobDetails::where('id',$request->order_id)->pluck('technician_id')->first();
            $technician_id=Technician::where('id',$technicianId)->pluck('user_id')->first();
            $technician=User::where('id',$technician_id)->where('user_type','technician')->select('lat','lng','name','phone','image','address')->first();
            $res_item['technician']=$technician;

            $subcategoryData=JobDetails::where('id',$request->order_id)->pluck('order_id')->first();
            $subcategory = Subcategory::where('id',$subcategoryData)->select('name_'.$lang. ' as name','price')->first();
            $res_item['subcategory']=$subcategory;

            $orderData=Order::where('id',$request->order_id)->select('area','block','street','house')->first();
            $res_item['order_details'] = $orderData;

            $Data=JobDetails::where('id',$request->order_id)->pluck('order_id')->first();
            $order=Order::where('id',$Data)->pluck('cart_id')->first();
            $orderCart=Cart::where('id',$order)->select('quantity','total','payment_type')->first();
            $res_item['cart']=$orderCart;




            $res_list[] = $res_item;
        }
        $response = [
            'message' =>'get data of job successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
