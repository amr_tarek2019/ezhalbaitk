<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RateTechnician extends Model
{
    protected $table='rate_technicians';
    protected $fillable=[ 'user_id', 'technician_id', 'order_id', 'rate'];
}
