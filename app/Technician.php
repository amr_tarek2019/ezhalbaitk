<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $table='technicians';
    protected $fillable=['user_id', 'category_id', 'subcategory_id', 'is_busy'];
}
