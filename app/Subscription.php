<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table='subscriptions';
    protected $fillable=['category_id', 'package_id', 'months', 'price','status','currency'];

    public function subscriptionsCategories()
    {
        return $this->hasManyThrough(Subscription::class, Category::class);
    }
}
