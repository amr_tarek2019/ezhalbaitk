<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobDetails extends Model
{
    protected $table='jobs_details';
    protected $fillable=['user_id', 'technician_id', 'order_id', 'status'];
}
