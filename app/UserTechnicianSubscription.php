<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTechnicianSubscription extends Model
{
    protected $table='users_technicians_subscriptions';
    protected $fillable=['user_subscription_id', 'technician_id'];
}
