<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table='notifications';
    protected $fillable=['user_id', 'order_id', 'text'];
}
